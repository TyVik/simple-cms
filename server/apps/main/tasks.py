from celery import shared_task

from server.apps.main.models import Page


@shared_task
def update_page_visit_counter(page_id: int) -> None:
    Page.consider_visit(page_id)
