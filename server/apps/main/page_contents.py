from typing import Dict, Type

from django.db import models
from rest_framework import serializers


class Registration(object):
    registration: Dict[Type[models.Model], Type[serializers.ModelSerializer]] = {}

    @staticmethod
    def register(serializer: Type[serializers.ModelSerializer]) -> None:
        Registration.registration[serializer.Meta.model] = serializer
