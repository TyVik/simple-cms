# -*- coding: utf-8 -*-

from django.urls import path
from rest_framework.routers import DefaultRouter

from server.apps.main.views import index, PagesViewSet

# Place your URLs here:

app_name = 'main'

router = DefaultRouter()
router.register(r'pages', PagesViewSet, 'pages')
urlpatterns = router.urls
urlpatterns += [
    path('hello', index, name='hello'),
]
