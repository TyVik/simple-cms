from django.core.management.base import BaseCommand
from django.db import transaction
from django.apps import apps
from django.db import connection


WARNING = '''\
Are you sure?
ALL DATA IN THE APP MODELS WILL BE LOST!
Continue? (y/N): '''


class Command(BaseCommand):
    help = 'Clean application models.'

    def handle(self, *args, **options):
        print(WARNING, end='')  # self.stdout.write is preferred
        value = input()
        if value.lower() != 'y':  # I'd make the required argument
            print('Cancelled.')
            return

        def turn_triggers(config, on):
            sql = 'ALTER TABLE "{}" ENABLE TRIGGER ALL;' if on else 'ALTER TABLE "{}" DISABLE TRIGGER ALL;'
            print(('Enabling' if on else 'Disabling') + ' constraints...')
            with connection.cursor() as cursor:
                for _, model in config.models.items():
                    cursor.execute(sql.format(model._meta.db_table))

        with transaction.atomic():
            app_config = apps.get_app_config('simplecms')
            turn_triggers(app_config, False)
            try:
                with connection.cursor() as cursor:
                    for model_name, model in app_config.models.items():
                        print(f'{model_name}: {model.objects.count()}')
                        cursor.execute('DELETE FROM {};'.format(model._meta.db_table))  # why you was not use ORM?!
            finally:
                turn_triggers(app_config, True)

        print('Done.')


