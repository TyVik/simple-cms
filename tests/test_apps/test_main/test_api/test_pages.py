from typing import List

import pytest
from django.http import HttpResponse

from server import settings
from server.apps.main.models import Page, Video, Text

from django.urls import reverse

import json


@pytest.mark.django_db
def test_pages(api_client):
    page = Page.objects.create(title='Test1')
    video = Video.objects.create(
        title='Fake video1',
        video_url='http://fake_video1',
        subtitles_url='http://fake_video1/subtitles'
    )
    page_contents = page.contents.create(order=1, content_object=video)
    assert 'http://fake_video1' in str(page_contents)
    assert 'http://fake_video1/subtitles' in str(page_contents)
    assert 'Fake video1' in str(page_contents)

    page = Page.objects.create(title='Test2')
    text = Text.objects.create(
        title='Fake text1',
        text='Lorem ipsum dolores met amet'
    )
    page_contents = page.contents.create(order=1, content_object=text)
    assert 'Fake text1' in str(page_contents)
    assert 'Lorem ipsum' in str(page_contents)

    url = reverse('pages-list')
    response = api_client.get(url)
    assert response.status_code == 200
    assert len(response.data['results']) == 2

    def get_detail(pages_list: List, page_title: str) -> HttpResponse:
        target = None
        for item in pages_list:
            if item['title'] == page_title:
                target = item
                break
        assert target
        result = api_client.get(target['details'])
        return result

    response = get_detail(response.data['results'], 'Test2')
    assert response.status_code == 200
    assert response.data['title'] == 'Test2'
    assert len(response.data['contents']) == 1
    content_object = response.data['contents'][0]['content_object']
    assert content_object['text'] == 'Lorem ipsum dolores met amet'
    assert settings.CELERY_TASK_ALWAYS_EAGER is True
    text_content = Text.objects.get(id=content_object['id'])
    assert text_content.visits_counter == 1
