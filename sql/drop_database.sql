/*
This file is used to drop development database;

Note: ONLY development database;
*/

DROP DATABASE simple_cms;
DROP USER simple_cms;
